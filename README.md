usage example:
```
deploy:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config use-context ketty/agent:agent-1
    - kubectl get pods
```
